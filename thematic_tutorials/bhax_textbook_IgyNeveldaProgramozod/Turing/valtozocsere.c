//Két változót cserél fel, segédváltozó nélkül.

#include<stdio.h>

int 
main()
{
    int a,b;
    a=6;
    b=13;
    
    printf("a=%d b=%d\n",a,b);    
    
    a=a-b;
    b=a+b;
    a=b-a;
    
    printf("a=%d b=%d\n",a,b);
        

    return 0;
}
