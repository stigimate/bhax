<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Stroustrup!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    
    <section>
        <title>JDK osztályok</title>
	<para>
            <emphasis role="italic">Írjunk olyan Boost C++ programot (indulj ki például a fénykardból) amely kilistázza a JDK összes
            osztályát (miután kicsomagoltuk az src.zip állományt, arra ráengedve)!
            A JDK osztályaihoz a fénykard ezt a részét használjuk fel:</emphasis>	    
        </para>
	<para>
	    <link xlink:href="https://github.com/nbatfai/future/blob/master/cs/F9F2/fenykard.cpp?fbclid=IwAR1ekbK8NPEjQVLD95T9I0KIw_FcX71ZeaRFU8fY0MA5gfKxAxUCPO-qSDo#L119">https://github.com/nbatfai/future/blob/master/cs/F9F2/fenykard.cpp</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/JDK">https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/JDK</link>             
        </para>
        <para>
            A <emphasis role="strong">JDK</emphasis>
            <emphasis role="italic">(Java Development Kit)</emphasis> Java fejlesztőknek szánt termék, mely tartalmazza a Java fordítót (javac), valamint egy Java futtató környezetet, azaz egy <emphasis role="strong">JRE</emphasis>-t <emphasis role="italic">(Java Runtime Environment)</emphasis>, továbbá különböző programozási eszközöket is.
            Ez a legnépszerűbb  <emphasis role="strong">SDK</emphasis> 
            <emphasis role="italic">(Software Development Kit)</emphasis>.
            1996-tól kezdődően több JDK verzió is készült, különböző kódnevekkel, mint például az Oak, a Playground, a Kestler, a Dolphin stb.
        </para>
        <para>
            A kért JDK forrásállományt az alábbi módon szerezhetjük be:
        </para>
        <screen>
stigimate@T450-Ubuntu:~/Asztal/ORA/Str$ wget https://download.java.net/java/GA/jdk13/5b8a42f3905b406298b72d750b6919f6/33/GPL/openjdk-13_linux-x64_bin.tar.gz 
        </screen>
        <para>
            Mivel ez egy tömörített állomány, ki kell csomagolnunk, majd megkeresni benne azt a fájlt, amire szükségünk lesz ez pedig az src.zip. (mivel .zip fájl: unzip src.zip)
            A kért program elkészítéséhez segítségül szolgálhat a FUTURE projekt fenykard.cpp állománya, ahol a <emphasis role="italic">read_acts</emphasis> függvény a <emphasis role="italic">.props</emphasis> fájlokat kikeresi és kilistázza.
        </para>
        <para>
            A boost.cpp program segítségével fogjuk kilistázni az imént kicsomagolt src mappából azokat az állományokat, melyeknek kiterjesztése <emphasis role="italic">.java</emphasis>, majd kiiratjuk a JDK osztályainak számát.
            Az egyszerűbb működés érdekében rekurzívan megyünk végig a könyvtáron.
        </para>
        <para>
            boost.cpp:
        </para>
        <para>
            <programlisting language='c++'><![CDATA[
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

class Feldolgoz {
    
private:
    std::string _path;
    int _count = 0;
    
public:
    Feldolgoz(std::string filePath):_path(filePath) {}
    
    void travel(boost::filesystem::path path) {
        boost::filesystem::directory_iterator it{path}, eod;
        BOOST_FOREACH(boost::filesystem::path const& p, std::make_pair(it, eod)) {
            if (boost::filesystem::is_regular_file(p) && boost::filesystem::extension(p.string()) == ".java") {
                std::cout << p << std::endl;
                _count++;
            }
            else if(boost::filesystem::is_directory(p)) travel(p);
        }
    }
    
    std::string getPath() {
        return _path;
    }
    
    int getCount() {
        return _count;
    }
    
};

void usage(){
    std::cout << "./boost <mappa>\n";
}

int main(int argc, char** argv){
    if (argc < 2) {
        usage();
        return -1;
    }

    Feldolgoz* obj = new Feldolgoz (argv[1]);
    obj->travel(obj->getPath());
    std::cout << obj->getCount() << std::endl;
}
    ]]>     </programlisting>
        </para>
        <para>
            Fordítás és futtatás:
        </para>
        <screen>
stigimate@T450-Ubuntu:~/Asztal/ORA$ g++ boost.cpp -o boost -lboost_system -lboost_filesystem -lboost_program_options -std=c++14   
stigimate@T450-Ubuntu:~/Asztal/ORA$ ./boost src     
        </screen>
        <para>
            És az eredmény:
        </para>
        <para>
            <figure>
            <title></title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../../Stroustrup/JDK/eredmeny.png" format="JPG" scale="45"/>
                </imageobject>
            </mediaobject>
            </figure>
        </para>
     
       
    </section>
    
     <section>
        <title>Másoló-mozgató szemantika</title>
	<para>
            <emphasis role="italic"> Kódcsipeteken (copy és move ctor és assign) keresztül vesd össze a C++11 másoló és a mozgató
            szemantikáját, a mozgató konstruktort alapozd a mozgató értékadásra!</emphasis>	   
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/Masolo_mozgato">https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/Masolo_mozgato</link>             
        </para>
        <para>
            Ezzel a témával már Prog1-en is foglalkoztunk. (A könyv első részében a Hello, Welch! című fejezetben olvashatunk róla)
            Korábban főleg az LZWBinFa program kapcsán volt róla szó, szóval most megpróbálok ettől elvonatkoztatni és másik példán/példákon keresztül bemutatni a másoló és a mozgató szemantikát.
            Kis angol tudással az alábbi linkek is hasznosak lehetnek számunkra a jobb megértés érdekében:
        </para>
        <para>
            <link xlink:href="https://en.cppreference.com/w/cpp/language/move_assignment">https://en.cppreference.com/w/cpp/language/move_assignment</link>
        </para> 
        <para>
            <link xlink:href="https://en.cppreference.com/w/cpp/language/copy_assignment">https://en.cppreference.com/w/cpp/language/copy_assignment</link>
        </para>
        <para>
            <link xlink:href="https://en.cppreference.com/w/cpp/language/move_constructor">https://en.cppreference.com/w/cpp/language/move_constructor</link>
        </para>
        <para> 
            <link xlink:href="https://en.cppreference.com/w/cpp/language/copy_constructor">https://en.cppreference.com/w/cpp/language/copy_constructor</link>    
        </para>
        <para>
            Először talán tisztázzunk néhány alapfogalmat.
            A konstruktor egy tagfüggvény, aminek a neve ugyanaz, mint az osztály neve, valamint megmondja, hogy egy (létrejövő) objektum attribútumai milyen kezdőértékekkel rendelkezzenek.
            A destruktor is egy tagfüggvény, és ő azért felel, hogy a lefoglalt erőforrások felszabaduljanak. Se a konstruktornak, se a destruktornak nincs visszatérési típusa.
            A másoló konsturktor és a másoló értékadás, mind fontos része az objektum-orientált programozásnak.
            Általuk már meglévő példányokból tudunk más példányokat létrehozni, a régi mintájára.
            A mozgató szemantikában pedig átruházódnak az erőforrások egyik objektumról a másikra, így az eredeti objektum elveszíti a tartalmát.         
        </para>
        <para>
            Ebben az egyszerű kis programban mindenre találunk példát:
        </para>
        <para>
            <programlisting language='c++'><![CDATA[
#include<iostream>  
#include<stdio.h>  
  
using namespace std;  
  
class Proba 
{  
private: int* ertek;
public: 
    Proba(int p) {
        ertek = new int(p);
        cout<<"Konstruktor"<<endl;
    }  
/*
    ~Proba() {
        delete ertek;
        cout<<"Destruktor"<<endl;
    }
*/    
    Proba(const Proba &p) : ertek (new int) {
        *ertek=*(p.ertek);
        cout<<"Másoló konstruktor"<<endl;  
    }  
      
    Proba& operator = (const Proba &t) 
    { 
        int *ertek2 = new int();
        *ertek2 = *(t.ertek);
        delete ertek;
        ertek = ertek2;
        cout<<"Másoló értékadás"<<endl; 
        return *this; 
    }  

    Proba (Proba && t) : ertek (nullptr) {
        *this = std::move(t);
        cout<<"Mozgató konstruktor"<<endl;
    }

    Proba& operator = (Proba && t){
        swap(ertek,t.ertek);
        cout<<"Mozgató értékadás"<<endl;
        return *this;
    }
    
};  
  
int main()  
{  
    Proba p1(2);
    Proba p2(4);
    Proba p3(6);
 
    Proba p4(p2);
    p3=p2;

    p4=std::move(p3);
    Proba p5=std::move(p2);
      
    return 0;  
}  
    ]]>     </programlisting>
        </para>
        <para>
            Fordítás és futtatás:
        </para>
        <para>
            <figure>
            <title></title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../../Stroustrup/Masolo_mozgato/eredmeny.png" format="JPG" scale="60"/>
                </imageobject>
            </mediaobject>
            </figure>
        </para>
        <para>
            Látható, hogy első esetben benne hagytam a destruktort és ezáltal az felszabadította a helyet, majd miután kikommenteltem nyilván az a rész már nem futott le.
            Valamint első esetben nem volt még benne a <emphasis role="italic">Proba p5=std::move(p2);</emphasis> sor.
        </para>
       
       
    </section> 
    
    <section>
        <title>Hibásan implementált RSA törése</title>
	<para>
            <emphasis role="italic"> Készítsünk betű gyakoriság alapú törést egy hibásan implementált RSA kódoló:</emphasis>	   
            <link xlink:href="https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_3.pdf">https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_3.pdf</link> 
            <emphasis role="italic">(71-73 fólia) által készített titkos szövegen.</emphasis>       
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/RSA">https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/RSA</link>             
        </para>
        <para>
            Tutor: <link xlink:href="https://gitlab.com/whoisZORZ/bhax/">Országh Levente</link>  
        </para>
        <para>
            Az RSA egy asszimetrikus titkosító algoritmus, amit 1976-ban fejlesztettek ki.
            Név szerint Ron Rivest, Adi Shamir és Len Adleman. (Az algoritmus neve az ő neveik kezdőbetűjéből ered)
            Az elméleti alapjait a moduláris számelmélet és a prímszámelmélet egyes tételei rejtik. 
            Az RSA egy ma is elterjedt és gyakran használt titkosítási eljárás.
            Ha részletesebben szeretnénk olvasni a témáról, akkor az alábbi oldalt ajánlom, ahol lépésről-lépésre leírják az algoritmus működését.
        </para>
        <para>
            <link xlink:href="https://e-szigno.hu/tudasbazis/rsa-algoritmus.html">https://e-szigno.hu/tudasbazis/rsa-algoritmus.html</link> 
        </para>
        <para>
            Előfordulhat, hogy egy-egy felhasználónak nem elég nagy a Long, vagy nem elég pontos a Float.
            Java-ban nekik hozták létre a BigInteger és a BigDecimal osztályokat.
            A BigDecimal egy bármekkora nagy egész értékből és egy nemnegatív 32-bites skálázó faktorból áll (szintén egész).
            Vegyük példának a 999.999-et, aminek egész értéke BigDecimal-ként 999999, a skálázó faktoré pedig 3. 
        </para>
        <para>
            Az alapötlet egy olyan algoritmus volt, amiben a kódolás, titkosítás folyamatát elválasztják a dekódolástól, azaz a kódoláshoz használt paraméterből ne lehessen meghatározni a dekódoláshoz szükséges paramétert.          
        </para>
        <para>
            Így néz ki a kulcsgenerálás:
        </para>
        <para>
            <programlisting language='java'><![CDATA[
public class RSA{
	public static void main(String[] args)
	{ 	
        int meretBitekben = (int) (700 * (int) (java.lang.Math.log( (double) 10)) / java.lang.Math.log((double) 2));
		
		System.out.println("Méret bitekben: ");
		System.out.println(meretBitekben);
		
		java.math.BigInteger p_i = new java.math.BigInteger(meretBitekben, 100, new java.util.Random() );
		
		System.out.println("p_i");
		System.out.println(p_i);
		System.out.println("p_i hexa");
		System.out.println(p_i.toString(16));
		
		java.math.BigInteger q_i = new java.math.BigInteger(meretBitekben, 100, new java.util.Random() );
		
		System.out.println("q_i");
		System.out.println(q_i);
		
		java.math.BigInteger m_i =p_i.multiply(q_i);
		
		System.out.println("m_i");
		System.out.println(m_i);
			
		java.math.BigInteger z_i = p_i.subtract(java.math.BigInteger.ONE).multiply(q_i.subtract(java.math.BigInteger.ONE));
		
		System.out.println("z_i");
		System.out.println(z_i);
		java.math.BigInteger d_i;
		do {
			do {
				d_i = new java.math.BigInteger(meretBitekben, new java.util.Random());
			} while(d_i.equals(java.math.BigInteger.ONE));
		} while(!z_i.gcd(d_i).equals(java.math.BigInteger.ONE));		
		
		System.out.println("d_i");
		System.out.println(d_i);
		java.math.BigInteger e_i = d_i.modInverse(z_i);
		
		System.out.println("e_i");
		System.out.println(e_i);
	}
}
    ]]>     </programlisting>
        </para>
        <para>
           Fordítás és futtatás:
        </para>
        <para>
            <figure>
            <title></title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../../Stroustrup/RSA/rsa.png" format="JPG" scale="50"/>
                </imageobject>
            </mediaobject>
            </figure>
        </para>
        <para>
            Magához a titkosításhoz egy titkos és egy publikus kulcsot használunk.
            A p és a q két (nagy) prímszám lesz, melyek értékét összeszorozva kapjuk meg a modulust.
        </para>
        <para>
            <programlisting language='java'><![CDATA[
public class RSA2{

static class KulcsPar{

  java.math.BigInteger d, e, m;

  public KulcsPar(){
      
      int meretBitekben = 700 * (int) (java.lang.Math.log((double) 10) / java.lang.Math.log((double) 2));

      java.math.BigInteger p = new java.math.BigInteger(meretBitekben, 100, new java.util.Random());

      java.math.BigInteger q = new java.math.BigInteger(meretBitekben, 100, new java.util.Random());

      m = p.multiply(q);

      java.math.BigInteger z = p.subtract(java.math.BigInteger.ONE).multiply(q.subtract(java.math.BigInteger.ONE));

      do {
        do {
            d = new java.math.BigInteger(meretBitekben, new java.util.Random());
          } while (d.equals(java.math.BigInteger.ONE));
        } while (!z.gcd(d).equals(java.math.BigInteger.ONE));
        
      e = d.modInverse(z);
    }
  }

public static void main(String[] args){

    KulcsPar jSzereplo = new KulcsPar();

    String tisztaSzoveg = "Utra kelunk. Megyunk az Oszbe, Vijjogva, sirva, kergetozve, Ket lankadt szarnyu heja-madar.";
    
    byte[] buffer = tisztaSzoveg.getBytes();
    java.math.BigInteger[] titkos = new java.math.BigInteger[buffer.length];

    for(int i = 0; i < titkos.length; i++)
      {
        titkos[i] = new java.math.BigInteger(new byte[]{buffer[i]});
        titkos[i] = titkos[i].modPow(jSzereplo.e, jSzereplo.m);
      }

    for(int i = 0; i < titkos.length; i++)
      {
        titkos[i] = titkos[i].modPow(jSzereplo.d, jSzereplo.m);
        buffer[i] = titkos[i].byteValue();
      }
    System.out.println(new String(buffer));
    }
  }

    ]]>     </programlisting>
        </para>
        <para>
            A titkos szöveget a tiszta szöveg ASCII kódjával feltöltjük. A modPow függvénynek megadjuk a hatványkitevőt, majd a modulust. (Ezáltal nagy integert kapunk)
            Visszafejtéskor pedig megint csak ezt a függvényt használjuk és most az első paraméter a kitevő inverze lesz, így ismét ASCII kód lesz a nagy integerből.
            Ezt betöltjük a buffer tömbbe és voilá megkapjuk a tiszta szöveget.
        </para>
        <para>
           Fordítás és futtatás:
        </para>
        <para>
            <figure>
            <title></title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../../Stroustrup/RSA/rsa2.png" format="JPG" scale="50"/>
                </imageobject>
            </mediaobject>
            </figure>
        </para>
       
    </section>
    
    <section>
        <title>Változó argumentumszámú ctor</title>
	<para>
            <emphasis role="italic">Készítsünk olyan példát, amely egy képet tesz az alábbi projekt Perceptron osztályának bemenetére
            és a Perceptron ne egy értéket, hanem egy ugyanakkora méretű „képet” adjon vissza. (Lásd még a 4
            hét/Perceptron osztály feladatot is.)</emphasis>	    
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/Ctor">https://gitlab.com/stigimate/bhax/tree/master/Stroustrup/Ctor</link>             
        </para>
        <para>
            Ha korábban még nem tettük meg, akkor most mindenképp telepítsük a libpng és a libpng++ könyvtárakat.
        </para>
        <screen>
            $ sudo apt-get install libpng-dev
            $ sudo apt-get install libpng++-dev
        </screen>
        <para>
            Ebben a feladatban felhasználunk néhány korábbi programot.
            Először előállítjuk újra a mandelbrotos programunkkal a mandel.png-t.
            Ez a kép lesz a bementünk. (A korábbi programok részletes magyarázata az adott kódot tárgyaló fejezetekben olvasható.)
        </para>
        <para>
            mandelpng.cpp:
        </para>
        <para>
            <programlisting language='c++'><![CDATA[
#include <iostream>
#include "png++/png.hpp"

int main (int argc, char *argv[])
{
    if (argc != 2) {
        std::cout << "Hasznalat: ./mandelpng fajlnev";
        return -1;
    }
    
    double a = -2.0, b = .7,  c = -1.35, d = 1.35;
    int szelesseg = 600, magassag = 600, iteraciosHatar = 1000;
        
    png::image <png::rgb_pixel> kep (szelesseg, magassag);
        
    double dx = (b-a)/szelesseg;
    double dy = (d-c)/magassag;
    double reC, imC, reZ, imZ, ujreZ, ujimZ;
    
    int iteracio = 0;
    std::cout << "Szamitas";
    
    for (int j=0; j<magassag; ++j) {
        for (int k=0; k<szelesseg; ++k) {
            
            reC = a+k*dx;
            imC = d-j*dy;
            
            reZ = 0;
            imZ = 0;
            iteracio = 0;
            
            while (reZ*reZ + imZ*imZ < 4 && iteracio < iteraciosHatar) {
                
                ujreZ = reZ*reZ - imZ*imZ + reC;
                ujimZ = 2*reZ*imZ + imC;
                reZ = ujreZ;
                imZ = ujimZ;
                
                ++iteracio;
                
            }        
            kep.set_pixel(k, j, png::rgb_pixel(255-iteracio%256,
                                               255-iteracio%256, 255-iteracio%256));
        }
        std::cout << "." << std::flush;
    }
    kep.write (argv[1]);
    std::cout << argv[1] << " mentve" << std::endl;
}
    ]]>     </programlisting>
        </para>
        <para>
            A kép előállítása:
        </para>
        <screen>
stigimate@T450-Ubuntu:~/Letöltések/bhax/Stroustrup/Ctor$ g++ mandelpng.cpp -o mandelpng -lpng
stigimate@T450-Ubuntu:~/Letöltések/bhax/Stroustrup/Ctor$ ./mandelpng mandel.png
Szamitas.........................mandel.png mentve      
        </screen>
        <para>
            Ez előállítja nekünk az alábbi képet:
        </para>
        <para>
            <figure>
            <title>mandel.png</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../../Stroustrup/Ctor/mandel.png" format="JPG" scale="50"/>
                </imageobject>
            </mediaobject>
            </figure>
        </para>
        <para>
            Az imént létrehozott képet fogjuk most feldolgozni. A Perceptronos feladatban tárgyalt alap program módosításaira lesz most szükségünk.        
        </para>
        <para>
            <programlisting language='c++'><![CDATA[
#include <iostream>
#include "perceptron.hpp"
#include "png++/png.hpp"

int main (int argc, char **argv)
{
    png::image <png::rgb_pixel> png_image (argv[1]);

    int size = png_image.get_width() * png_image.get_height();
    
    Perceptron* p = new Perceptron (3, size, 256, size);
    
    double* image = new double[size];
    
    for (int i = 0; i<png_image.get_width(); ++i)
        for (int j = 0; j<png_image.get_height(); ++j)
            image[i*png_image.get_width() + j] = png_image[i][j].red;

    double* newPNG = new double[size];

    for(int i = 0; i < png_image.get_width(); ++i)
        for(int j = 0; j<png_image.get_height(); ++j)
            png_image[i][j].green = newPNG[j*png_image.get_width()+j];
    png_image.write("output.png");   
  
    
    delete p;
    delete [] image;  
}
    ]]>     </programlisting>
        </para>
         <para>
            Az eredeti programban a Perceptron csak egy értéket ad vissza, de most a teljes a képet.
            Módosításokat kell végeznünk a header fájlon is, mivel új képet akarunk előállítani.
            A double pointer() operátor már egy tömböt ad vissza, ezzel már hozzá tudunk piszkálni a képhez.
            Az utolsó units tömbnek az értékei bekerülnek a paraméterként kapott tömbbe.
            A két kép (a régi és az új) azonos méretűek lesznek.
        </para>
        <para>
            <programlisting language='c++'><![CDATA[
double* operator() ( double image [] )
{
    units[0] = image;

    for ( int i {1}; i < n_layers; ++i )
      {

#ifdef CUDA_PRCPS

        cuda_layer ( i, n_units, units, weights );

#else

        #pragma omp parallel for
        for ( int j = 0; j < n_units[i]; ++j )
          {
            units[i][j] = 0.0;

            for ( int k = 0; k < n_units[i-1]; ++k )
              {
                units[i][j] += weights[i-1][j][k] * units[i-1][k];
              }

            units[i][j] = sigmoid ( units[i][j] );
          }
#endif
      }

    for (int i = 0; i < n_units[n_layers - 1]; i++)
        image[i] = units[n_layers - 1][i];
    
    return image;
}
    ]]>     </programlisting>
        </para>
        <para>
            Fordítás és futtatás:
        </para>
        <screen>
stigimate@T450-Ubuntu:~/Letöltések/bhax/Stroustrup/Ctor$ g++ perceptron.hpp main2.cpp -o main -lpng -std=c++11
stigimate@T450-Ubuntu:~/Letöltések/bhax/Stroustrup/Ctor$ ./main mandel.png
stigimate@T450-Ubuntu:~/Letöltések/bhax/Stroustrup/Ctor$ ls
main*  main2.cpp  mandelpng*  mandel.png  mandelpng.cpp  output.png  perceptron.hpp            
        </screen>
        <para>
            Láthatjuk, hogy létrejött az output.png
        </para>
        <para>
            <figure>
            <title>output.png</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="../../Stroustrup/Ctor/output.png" format="JPG" scale="65"/>
                </imageobject>
            </mediaobject>
            </figure>
        </para>
        
    </section>                              
        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
