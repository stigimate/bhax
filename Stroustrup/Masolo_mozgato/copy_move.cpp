#include<iostream>  
#include<stdio.h>  
  
using namespace std;  
  
class Proba 
{  
private: int* ertek;
public: 
    Proba(int p) {
	ertek = new int(p);
	cout<<"Konstruktor"<<endl;
    }  
/*
    ~Proba() {
	delete ertek;
	cout<<"Destruktor"<<endl;
    }
*/    
    Proba(const Proba &p) : ertek (new int) {
	*ertek=*(p.ertek);
        cout<<"Másoló konstruktor"<<endl;  
    }  
      
    Proba& operator = (const Proba &t) 
    { 
	int *ertek2 = new int();
	*ertek2 = *(t.ertek);
	delete ertek;
	ertek = ertek2;
        cout<<"Másoló értékadás"<<endl; 
        return *this; 
    }  

    Proba (Proba && t) : ertek (nullptr) {
    	*this = std::move(t);
	cout<<"Mozgató konstruktor"<<endl;
    }

    Proba& operator = (Proba && t){
	swap(ertek,t.ertek);
	cout<<"Mozgató értékadás"<<endl;

	return *this;
    }
    
};  
  
int main()  
{  
    Proba p1(2);
    Proba p2(4);
    Proba p3(6);
 
    Proba p4(p2);
    p3=p2;

    p4=std::move(p3);
    Proba p5=std::move(p2);
      
    return 0;  
}  
