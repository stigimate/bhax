package model;


/**
* @generated
*/
public class Jármű {
    
    /**
    * @generated
    */
    private String rendszám;
    
    /**
    * @generated
    */
    private String tulajdonos;
    
    
    
    /**
    * @generated
    */
    public String getRendszám() {
        return this.rendszám;
    }
    
    /**
    * @generated
    */
    public String setRendszám(String rendszám) {
        this.rendszám = rendszám;
    }
    
    /**
    * @generated
    */
    public String getTulajdonos() {
        return this.tulajdonos;
    }
    
    /**
    * @generated
    */
    public String setTulajdonos(String tulajdonos) {
        this.tulajdonos = tulajdonos;
    }
    

    //                          Operations                                  
    
    
}
