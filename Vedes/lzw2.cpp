#include <iostream>
#include <cmath>
#include <fstream>

int egydb=28, nuldb=15;
int db0=0, db1=0;

int val1=egydb, val2=nuldb; 

class LZWBinFa
{
public:
    
    LZWBinFa ()
    {
        gyoker = new Csomopont ('/');
        fa = gyoker;
    }
    LZWBinFa (const LZWBinFa & forras):LZWBinFa()
    {
        std::cout<<"Masolso konstruktor"<<std::endl;
        if (gyoker != nullptr)
        {
            szabadit(gyoker);
            std::cout<<"Masolso konstruktor"<<std::endl;
            gyoker = copy(forras.gyoker, forras.fa);
         
        }
    }
    LZWBinFa& operator=(const LZWBinFa& forras) 
    {     
        if (forras.gyoker == nullptr)
            return *this;
            
        szabadit(gyoker);
        gyoker = copy(forras.gyoker,forras.fa);
        return *this;
    }
    LZWBinFa (LZWBinFa&& forras)
    {
        std::cout<<"Move ctor\n";
        gyoker = nullptr;
        *this = std::move(forras);
        
    }
    LZWBinFa& operator= (LZWBinFa&& forras)
    {
        std::cout<<"Move assignment ctor\n";
        std::swap(gyoker, forras.gyoker);
        return *this;
    }
    ~LZWBinFa ()
    {
        szabadit (gyoker);
    }

    
    void operator<< (char b)
    {
        if (b == '0')
        {

            if (!fa->nullasGyermek ())
            {

                Csomopont *uj = new Csomopont ('0');
 
                fa->ujNullasGyermek (uj);

                fa = gyoker;
            }
            else
            {

                fa = fa->nullasGyermek ();
            }
        }
        else
        {
            if (!fa->egyesGyermek ())
            {
                Csomopont *uj = new Csomopont ('1');
                fa->ujEgyesGyermek (uj);
                fa = gyoker;
            }
            else
            {
                fa = fa->egyesGyermek ();
            }
        }
    }

    void kiir (void)
    {

        melyseg = 0;
        kiir (gyoker, std::cout);
    }
    int getMelyseg (void);
    int szamol (void);


    friend std::ostream & operator<< (std::ostream & os, LZWBinFa & bf)
    {
        bf.kiir (os);
        return os;
    }
    void kiir (std::ostream & os)
    {
        melyseg = 0;
        kiir (gyoker, os);
    }

private:
    class Csomopont
    {
    public:
        Csomopont (char b = '/'):betu (b), balNulla (0), jobbEgy (0)
        {
        };
        ~Csomopont ()
        {
        };
        Csomopont *nullasGyermek () const
        {
            return balNulla;
        }
        Csomopont *egyesGyermek () const
        {
            return jobbEgy;
        }
        void ujNullasGyermek (Csomopont * gy)
        {
            balNulla = gy;
        }

        void ujEgyesGyermek (Csomopont * gy)
        {
            jobbEgy = gy;
        }

        char getBetu () const
        {
            return betu;
        }

    private:
        char betu;
        Csomopont *balNulla;
        Csomopont *jobbEgy;
        Csomopont (const Csomopont &);
        Csomopont & operator= (const Csomopont &);
    };

    Csomopont *fa;
    int melyseg;
    void kiir (Csomopont * elem, std::ostream & os)
    {
        if (elem != NULL)
        {
            ++melyseg;
            kiir (elem->egyesGyermek (), os);
            for (int i = 0; i < melyseg; ++i)
                os << "---";
            os << elem->getBetu () << "(" << melyseg - 1 << ")" << std::endl;
            kiir (elem->nullasGyermek (), os);
            --melyseg;
        }
    }
    Csomopont* copy (const Csomopont* forras, const Csomopont *regifa )
    {
        Csomopont* masolt = nullptr;
        if (forras != nullptr)
        {
            masolt = new Csomopont(forras->getBetu());
            
            masolt->ujEgyesGyermek(copy(forras->egyesGyermek(), regifa));
            
            masolt->ujNullasGyermek(copy(forras->nullasGyermek(),regifa));
            
            if (regifa == forras)
                fa = masolt;
        }
        return masolt;
    }
    void szabadit (Csomopont * elem)
    {
        if (elem != NULL)
        {
            szabadit (elem->egyesGyermek ());
            szabadit (elem->nullasGyermek ());
            delete elem;
        }
    }

protected:
    Csomopont *gyoker;
    int maxMelyseg;

    void rmelyseg (Csomopont * elem);

};




int
LZWBinFa::getMelyseg (void)
{
    melyseg = maxMelyseg = 0;
    rmelyseg (gyoker);
    return maxMelyseg - 1;
}

void
LZWBinFa::rmelyseg (Csomopont * elem)
{
    if (elem != NULL)
    {
        ++melyseg;
        if (melyseg > maxMelyseg)
            maxMelyseg = melyseg;
        rmelyseg (elem->egyesGyermek ());
        rmelyseg (elem->nullasGyermek ());
        --melyseg;
    }
}


void szamol()
{
    
    while(val1 !=0)
    {
        val1=val1-db1;
        db1++;
    }
    while(val2 !=0)
    {
        val2=val2-db0;
        db0++;
    }
}

int
main (int argc, char *argv[])
{
    LZWBinFa egyesfa;
    LZWBinFa nullasfa;

    for(int i = 0; i < egydb ; i++)
    {
	egyesfa << '1';		
    }  


    for(int i = 0; i < nuldb ; i++)
    {
 	nullasfa << '0';
    }
    
    szamol();


    std::cout << "Csupa nullabol allo fa: "<< std::endl << nullasfa << std::endl;
   
    std::cout << std::endl;    

    std::cout << "Csupa egyesbol allo fa: " << std::endl << egyesfa << std::endl;
    
    std::cout << "A 0-as faban : " << db0-1  <<" db 0 van."<< std::endl;  //a -1 a gyökér miatt kell
    std::cout << "Az 1-es faban : " << db1-1  <<" db 1 van."<< std::endl;  //a -1 a gyökér miatt kell

    
    return 0;
}

